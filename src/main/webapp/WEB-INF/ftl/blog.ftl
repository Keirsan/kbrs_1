<#-- @ftlvariable name="_csrf" type="org.springframework.security.web.csrf.CsrfToken" -->
<#-- @ftlvariable name="form" type="eu.kielczewski.example.domain.UserCreateForm" -->
<#import "/spring.ftl" as spring>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Create a new user</title>
</head>
<body>
<nav role="navigation">
    <ul>
        <li><a href="/">Home</a></li>
    </ul>
</nav>

<h1>List blog</h1>

<p>${blog.name}</p>
<table>
    <thead>
    <tr>
        <th>Messages</th>
    </tr>
    </thead>
    <tbody>
    <#list blog.messages as message>
    <tr>
        <td>${message.message}</td>
    </tr>
    </#list>
    </tbody>
</table>

<form role="form" name="form" action="/blog/${blog.id}" method="post">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

    <div>
        <label for="message">message</label>
        <input type="text" name="message" id="message" required/>
    </div>
    <div>
        <input type="hidden" name="blogId" id="blogId" value="${blog.id}" required/>
    </div>
    <button type="submit">Save</button>
</form>

</body>
</html>