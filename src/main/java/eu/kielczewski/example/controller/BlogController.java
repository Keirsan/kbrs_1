package eu.kielczewski.example.controller;

import eu.kielczewski.example.domain.Blog;
import eu.kielczewski.example.domain.Message;
import eu.kielczewski.example.domain.MessageCreateForm;
import eu.kielczewski.example.domain.validator.UserCreateFormValidator;
import eu.kielczewski.example.repository.BlogRepository;
import eu.kielczewski.example.repository.MessageRepository;
import eu.kielczewski.example.service.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class BlogController {

    private static final Logger LOGGER = LoggerFactory.getLogger(BlogController.class);
    private final UserService userService;
    private final BlogRepository blogRepository;
    private final MessageRepository messageRepository;

    @Autowired
    public BlogController(UserService userService, UserCreateFormValidator userCreateFormValidator, BlogRepository blogRepository, MessageRepository messageRepository) {
        this.userService = userService;
        this.blogRepository = blogRepository;
        this.messageRepository = messageRepository;
    }

    @PreAuthorize("@currentUserServiceImpl.canReadBlog(principal, #id)")
    @RequestMapping("/blog/{id}")
    public ModelAndView getBlogPage(@PathVariable Long id) {
        LOGGER.debug("Getting blog page for user={}", id);
        return new ModelAndView("blog", "blog", blogRepository.getOne(id)
                );
    }

    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('USER')")
    @RequestMapping("/blogs")
    public ModelAndView getBlogs() {
        //LOGGER.debug("Getting blog page for user={}", id);
        return new ModelAndView("blogs", "blogs", blogRepository.findAll()
        );
    }

    @PreAuthorize("@currentUserServiceImpl.canWriteBlog(principal, #id)")
    @RequestMapping(value = "/blog/{id}", method = RequestMethod.POST)
    public String getBlogPage(@PathVariable Long id,@ModelAttribute("form") MessageCreateForm form) {
        //LOGGER.debug("Getting blog page for user={}", id);
        Message message = new Message();
        Blog blog = new Blog();
        blog.setId(Long.parseLong(form.getBlogId()));
        message.setBlog(blog);
        message.setMessage(form.getMessage());
        messageRepository.save(message);
        return "redirect:/blog/"+id;
    }

}
