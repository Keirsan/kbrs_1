package eu.kielczewski.example.repository;

import eu.kielczewski.example.domain.Blog;
import eu.kielczewski.example.domain.Message;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MessageRepository extends JpaRepository<Message, Long> {

}
