<#-- @ftlvariable name="user" type="eu.kielczewski.example.domain.User" -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>User details</title>
</head>
<body>
<nav role="navigation">
    <ul>
        <li><a href="/">Home</a></li>
    </ul>
</nav>

<h1>User details</h1>

<p>E-mail: ${user.email}</p>

<p>Role: ${user.role}</p>

<table>
    <thead>
    <tr>
        <th>Permission</th>
        <th>Blog</th>
    </tr>
    </thead>
    <tbody>
    <#list user.userHasPermissionList as perms>
    <tr>
        <td>${perms.permission.name}</td>
        <td>${perms.pk.blog.name}</td>

    </tr>
    </#list>
    </tbody>
</table>

<form role="form" name="perm_form" action="/permission" method="post">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

    <div>
        <label for="blogId">blogId</label>
        <input type="text" name="blogId" id="blogId" required autofocus/>
    </div>
    <div>
        <input type="hidden" name="userId" id="userId" value="${user.id}" required/>
    </div>
    <div>
        <label for="permissionId">permissionId</label>
        <select name="permissionId" id="permissionId" required>
            <option value="1">READ</option>
            <option value="2">WRITE</option>
        </select>
    </div>
    <button type="submit">Save</button>
</form>

</body>
</html>