package eu.kielczewski.example.domain;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

public class PermissionCreateForm {

    @NotEmpty
    private String blogId = "";

    @NotEmpty
    private String userId = "";

    @NotEmpty
    private String permissionId = "";

    public String getBlogId() {
        return blogId;
    }

    @Override
    public String toString() {
        return "PermissionCreateForm{" +
                "blogId='" + blogId + '\'' +
                ", userId='" + userId + '\'' +
                ", permissionId='" + permissionId + '\'' +
                '}';
    }

    public void setBlogId(String blogId) {
        this.blogId = blogId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(String permissionId) {
        this.permissionId = permissionId;
    }
}
