package eu.kielczewski.example.domain;

import org.hibernate.validator.constraints.NotEmpty;

public class MessageCreateForm {

    @NotEmpty
    private String blogId = "";

    @NotEmpty
    private String message = "";

    public String getBlogId() {
        return blogId;
    }

    public void setBlogId(String blogId) {
        this.blogId = blogId;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "MessageCreateForm{" +
                "blogId='" + blogId + '\'' +
                ", message='" + message + '\'' +
                '}';
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
