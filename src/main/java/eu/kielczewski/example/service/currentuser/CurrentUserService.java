package eu.kielczewski.example.service.currentuser;

import eu.kielczewski.example.domain.CurrentUser;

public interface CurrentUserService {

    boolean canAccessUser(CurrentUser currentUser, Long userId);

    boolean canReadBlog(CurrentUser currentUser, Long blogId);

    boolean canWriteBlog(CurrentUser currentUser, Long blogId);
}
