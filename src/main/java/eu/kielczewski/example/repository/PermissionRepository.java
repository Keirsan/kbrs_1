package eu.kielczewski.example.repository;

import eu.kielczewski.example.domain.User;
import eu.kielczewski.example.domain.UserHasPermission;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PermissionRepository extends JpaRepository<UserHasPermission, Long> {

}
