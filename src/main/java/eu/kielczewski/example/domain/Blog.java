package eu.kielczewski.example.domain;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "blog")
public class Blog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    private Long id;

    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @OneToMany(mappedBy = "blog", fetch = FetchType.EAGER)
    private List<Message> messages;

    public Long getId() {
        return id;
    }

  @Override
    public String toString() {
        return "Blog{" +
                "id=" + id +
                ", name='" + name +
                '}';
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }
}
