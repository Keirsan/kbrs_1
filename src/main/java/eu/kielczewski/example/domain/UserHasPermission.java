package eu.kielczewski.example.domain;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "user_has_permission")
@AssociationOverrides({
        @AssociationOverride(name = "pk.blog",
                joinColumns = @JoinColumn(name = "blog_id")),
        @AssociationOverride(name = "pk.user",
                joinColumns = @JoinColumn(name = "user_id"))})
public class UserHasPermission {

    @EmbeddedId
    private UserHasPermissionPK pk = new UserHasPermissionPK();

    @Override
    public String toString() {
        return "UserHasPermission{" +
                "pk=" + pk +
                ", blog=" + permission +
                '}';
    }

    @ManyToOne
    @JoinColumn(name = "permission_id")
    private Permission permission;

    public UserHasPermissionPK getPk() {
        return pk;
    }

    public void setPk(UserHasPermissionPK pk) {
        this.pk = pk;
    }

    public Permission getPermission() {
        return permission;
    }

    public void setPermission(Permission permission) {
        this.permission = permission;
    }
}
